﻿using System.Threading.Tasks;
using PairingTest.Web.Models;
using QuestionServiceWebApi;
using QuestionServiceWebApi.Interfaces;

namespace PairingTest.Unit.Tests.QuestionServiceWebApi.Stubs
{
    public class FakeQuestionRepository : IQuestionRepository
    {
        public Questionnaire ExpectedQuestions { get; set; }
        public Questionnaire GetQuestionnaire()
        {
            return ExpectedQuestions;
        }

        public bool SaveAnswer(AnswerDto answerDto)
        {
            return true;
        }
    }
}