﻿using Xunit;
using PairingTest.Unit.Tests.QuestionServiceWebApi.Stubs;
using QuestionServiceWebApi;
using QuestionServiceWebApi.Controllers;
using PairingTest.Web.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PairingTest.Unit.Tests.QuestionServiceWebApi
{
    public class QuestionsControllerTests
    {
        [Fact]
        public void ShouldGetQuestions()
        {
            //Arrange
            var expectedTitle = "My expected questions";
            var expectedQuestions = new Questionnaire() {QuestionnaireTitle = expectedTitle};
            var fakeQuestionRepository = new FakeQuestionRepository() {ExpectedQuestions = expectedQuestions};
            var questionsController = new QuestionsController(fakeQuestionRepository);

            //Act
            var questions = questionsController.Get();

            //Assert
            Assert.Equal(questions.QuestionnaireTitle, expectedTitle);
        }

        [Fact]
        public void ShouldPostAnswer()
        {
            //Arrange
            var expectedTitle = "My expected questions";
            var expectedQuestions = new Questionnaire() { QuestionnaireTitle = expectedTitle };
            var fakeQuestionRepository = new FakeQuestionRepository() { ExpectedQuestions = expectedQuestions };
            var questionsController = new QuestionsController(fakeQuestionRepository);

            //Act
            var answerDto = new AnswerDto
            {
                AnswerId = "1",
                QuestionId = 1
            };

            var postOkResult = questionsController.Post(answerDto) as OkObjectResult;

            //Assert
            Assert.NotNull(postOkResult);
            Assert.True(postOkResult.StatusCode == 200);
        }
    }
}