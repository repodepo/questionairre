﻿using Xunit;
using QuestionServiceWebApi;
using PairingTest.Web.Models;

namespace PairingTest.Unit.Tests.QuestionServiceWebApi
{
    public class QuestionRepositoryTests
    {
        [Fact]
        public void ShouldGetExpectedQuestionnaire()
        {
            var questionRepository = new QuestionRepository();

            var questionnaire = questionRepository.GetQuestionnaire();

            Assert.Equal("Geography Questions", questionnaire.QuestionnaireTitle);
            Assert.Equal("What is the capital of Cuba?", questionnaire.Questions[0]);
            Assert.Equal("What is the capital of Germany?", questionnaire.Questions[1]);
            Assert.Equal("What is the capital of France?", questionnaire.Questions[2]);
            Assert.Equal("What is the capital of Poland?", questionnaire.Questions[3]);
        }

        [Theory]
        [InlineData("Havana",0)]
        [InlineData("Berlin", 1)]
        [InlineData("Paris", 2)]
        [InlineData("Warsaw", 3)]
        public void ShouldReturnAnswerAsTrue(string answerId, int questionId)
        {
            var questionRepository = new QuestionRepository();

            var answerDto = new AnswerDto
            {
                AnswerId = answerId,
                QuestionId = questionId
            };

            var isCorrect = questionRepository.CheckOption(answerDto);

            Assert.True(isCorrect);
        }
    }
}