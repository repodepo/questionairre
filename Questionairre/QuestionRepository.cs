using System.Collections.Generic;
using System.Threading.Tasks;
using PairingTest.Web.Models;
using QuestionServiceWebApi.Interfaces;
using System;

namespace QuestionServiceWebApi
{
    public class QuestionRepository : IQuestionRepository
    {
        public Questionnaire GetQuestionnaire()
        {
            var questionnaire = new Questionnaire
            {
                QuestionnaireTitle = "Geography Questions",
                Questions = new Dictionary<int, string>(),
                AnswerOptions = new Dictionary<int, List<string>>()
            };
         
            questionnaire.Questions.Add(0, "What is the capital of Cuba?");
            questionnaire.AnswerOptions.Add(0, new List<string>{ "Havana", "Rome", "Phuket", "Swindon"});

            questionnaire.Questions.Add(2, "What is the capital of France?");
            questionnaire.AnswerOptions.Add(2, new List<string> { "Brighton", "Moscow", "Paris", "Scunthorpe" });

            questionnaire.Questions.Add(3, "What is the capital of Poland?");
            questionnaire.AnswerOptions.Add(3, new List<string> { "Madagascar", "Amsterdam", "Warsaw", "Swindon" });

            questionnaire.Questions.Add(1, "What is the capital of Germany?");
            questionnaire.AnswerOptions.Add(1, new List<string> { "Cairo", "Rome", "Phuket", "Berlin" });

            return questionnaire;
        }

        public bool SaveAnswer(AnswerDto answerDto)
        {
            // Save answer in db

            return CheckOption(answerDto);
        }

        public bool CheckOption(AnswerDto answerDto)
        {
            switch (answerDto.QuestionId)
            {
                case 0:
                    return Convert.ToBoolean(answerDto.AnswerId == "Havana");
                case 1:
                    return Convert.ToBoolean(answerDto.AnswerId == "Berlin");
                case 2:
                    return Convert.ToBoolean(answerDto.AnswerId == "Paris");
                case 3:
                    return Convert.ToBoolean(answerDto.AnswerId == "Warsaw");
                default:
                    return false;
            }
        }
    } 
}