import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Questionnaire } from './components/Questionnaire';

export const routes = <Layout>
    <Route exact path='/' component={ Questionnaire } />
</Layout>;
