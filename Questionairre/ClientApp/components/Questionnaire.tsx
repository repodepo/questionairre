﻿import * as React from 'react';
import 'bootstrap';

import { RouteComponentProps } from 'react-router';
import 'isomorphic-fetch';
import { EventHandler } from 'react';

interface QuestionnaireState {
    questionnaire: QuestionnaireModel,
    loading: boolean;
    showAnswers: false;
}

interface CheckAnswersButtonProps {
    Questionnaire: Questionnaire,
}

export class Questionnaire extends React.Component<RouteComponentProps<{}>, any> {

    constructor() {
        super();

        this.state = {
            questionnaire: { questionnaireTitle: '', questions: new Map(), answerOptions: new Map() }, loading: true, showAnswers: false };

        fetch('api/Questions')
            .then(response => response.json() as Promise<QuestionnaireModel>)
            .then(data => {
                this.setState({ questionnaire: data, loading: false });
            });

        this.postAnswer = this.postAnswer.bind(this);
    }

    public render() {
        let contents = this.state.loading ? <p><em>Loading questions...</em></p> : this.renderBody();

        return <div>
                {contents}
               </div>;
    }

    private renderBody() {
        return <div className="row" id="geographyBackground">
            <div className="col-lg-4"></div>
            <div className="col-lg-4" id="questionnaireTableCol"><h2>{this.state.questionnaire.questionnaireTitle}</h2>{this.renderQuestionsTable(this.state.questionnaire)}
                <div className="row"><div className="col-lg-4" /><div className="col-lg-4"><CheckAnswersButton Questionnaire={this}/></div><div className="col-lg-4"></div></div>
            </div>
            <div className="col-lg-4"></div>
        </div>;
    }

    private renderCorrectOrFalse(option: string, id: number) {

        const correct = this.state[option + id];

        if (correct == null || undefined) {
            return '';
        }

        if (correct) {
            return `✔️`;
        }

        return `❌`;
    }

    private buttonRenderOptions(option: string, id: number): {} {
       
        return (
            <div><input type="radio" disabled={this.state.showAnswers} key={option + id} name={id.toString()} value={id} onClick={event => this.postAnswer(event, option, id)} /><label htmlFor={option + id}>{option}</label><label>{this.state.showAnswers ? this.renderCorrectOrFalse(option, id) : null}</label></div>
        )
    }

    public postAnswer(event: any, answerId: string, questionId: number) : any {

        const answerDto = new AnswerDto();
        answerDto.answerId = answerId;
        answerDto.questionId = questionId;

        fetch('api/Questions', {
            method: 'POST', 
            body: JSON.stringify(answerDto),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        .then(response => response.json() as Promise<boolean>)
            .then(data => {
                    this.setState({ [answerId + questionId]: data });
        });
    }

    showAnswers() {
        this.setState({ showAnswers: !this.state.showAnswers });
    }

    private renderQuestionsTable(questionnaire: QuestionnaireModel) {

        const questionAnswerRows = Object.values(questionnaire.questions).map((question : string, questionNumber : number) =>
                                <tr key={questionNumber}>
                                    <td>{question}</td>
                                <td>{Object.values(questionnaire.answerOptions).map((options: Array<string>, questionId: number) =>
                                    options.map(option => this.buttonRenderOptions(option, questionId)).filter(relevantQuestion => questionId === questionNumber))}</td>
                                </tr>);

        return <div>
                    <table className="table table-striped table-responsive">
                        <thead className="thead-inverse">
                            <tr>
                                <th>Question</th>
                                <th>Possible Answers</th>
                            </tr>
                        </thead>
                        <tbody>
                            {questionAnswerRows}
                        </tbody>
                    </table>
                </div>; 
    }
}


export class CheckAnswersButton extends React.Component<CheckAnswersButtonProps, any> {

    render() {
        return <button id="checkAnswerButton" onClick={event => this.props.Questionnaire.showAnswers()} >{!this.props.Questionnaire.state.showAnswers ? "Check My Answers!" : "Retry"}</button >;
    }
}

interface QuestionnaireModel {
    questionnaireTitle: string;
    questions: Map<number, string>;
    answerOptions: Map<number, Array<string>>;
}

class AnswerDto {
    questionId: number;
    answerId: string;
}
