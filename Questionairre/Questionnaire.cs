﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QuestionServiceWebApi
{
    [DataContract]
    public class Questionnaire
    {
        [DataMember]
        public string QuestionnaireTitle { get; set; }
        [DataMember]
        public Dictionary<int, string> Questions { get; set; }
        [DataMember]
        public Dictionary<int, List<string>> AnswerOptions { get; set; }
    }
}