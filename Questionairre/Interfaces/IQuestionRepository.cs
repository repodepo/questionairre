using System.Threading.Tasks;
using PairingTest.Web.Models;

namespace QuestionServiceWebApi.Interfaces
{
    public interface IQuestionRepository
    {
        Questionnaire GetQuestionnaire();
        bool SaveAnswer(AnswerDto answerDto);
    }
}