﻿using Microsoft.AspNetCore.Mvc;
using PairingTest.Web.Models;
using QuestionServiceWebApi.Interfaces;
using System.Threading.Tasks;

namespace QuestionServiceWebApi.Controllers
{
    [Route("api/[controller]")]
    public class QuestionsController : Controller
    {
        private readonly IQuestionRepository _questionRepository;

        public QuestionsController(IQuestionRepository questionRepository)
        {
            _questionRepository = questionRepository;
        }

        [HttpGet]
        public Questionnaire Get()
        {
            return _questionRepository.GetQuestionnaire();
        }

        [HttpGet]
        [Route("{id}")]
        public string Get(int id)
        {
            return string.Empty;
        }

        [HttpPost]
        public IActionResult Post([FromBody]AnswerDto answerDto)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult("Invalid Answer sent");
            }

            var isCorrect = _questionRepository.SaveAnswer(answerDto);
            return Ok(isCorrect);
        }

        [HttpPut]
        [Route("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
        }
    }
}
