﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PairingTest.Web.Models
{
    public class AnswerDto
    {
        [Required]
        public int QuestionId { get; set; }
        [Required]
        public string AnswerId { get; set; }
    }
}